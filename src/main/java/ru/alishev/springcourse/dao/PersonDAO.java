package ru.alishev.springcourse.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.alishev.springcourse.models.Person;

import java.util.List;


@Component
public class PersonDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public PersonDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional(readOnly = true)
    public List<Person> index() {
        Session session = sessionFactory.getCurrentSession();

        List<Person> people = session.createQuery("select p from Person p", Person.class)
                .getResultList();

        return people;
    }

    @Transactional
    public Person show(int id) {
        Session session = sessionFactory.getCurrentSession();
        return  session.get(Person.class, id);
    }


    @Transactional
    public void save(Person person) {
        Session session = sessionFactory.getCurrentSession();

        //ToDo вариант 1
        //Person createPerson = new Person(person.getName(), person.getAge());
        //session.save(createPerson);

        //ToDo вариант 2
        session.save(person);
    }

    @Transactional
    public void update(int id, Person updatedPerson) {
        Session session = sessionFactory.getCurrentSession();

        //ToDo вариант 1
        Person afterUpdatePerson = session.get(Person.class, id);
        afterUpdatePerson.setName(updatedPerson.getName());
        afterUpdatePerson.setAge(updatedPerson.getAge());
        session.save(afterUpdatePerson);

    }

    @Transactional
    public void delete(int id) {
        Session session = sessionFactory.getCurrentSession();

        session.remove(session.get(Person.class, id));
    }
}
